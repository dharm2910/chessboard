package Chess;

/**
 * Created by thakurd on 14/02/21.
 */
public class MoveDeterminer {


    public static boolean isLeftColumnExistForMove(Pair pair, int moveCount) {
        return pair.getColumn() + moveCount <= Board.getMaxcolumn();
    }


    public static boolean isRightColumnExistForMove(Pair pair, int moveCount) {
        return pair.getColumn() - moveCount >= Board.getStartColumn();
    }


    public static boolean isForwardRowExistForMove(Pair pair, int moveCount) {
        return pair.getRow() + moveCount <= Board.getMaxrow();
    }


    public static boolean isBackwardExistForMove(Pair pair, int moveCount) {
        return pair.getRow() - moveCount >= Board.getStartRow();
    }

    public static int moveLeftward(Pair pair, int moveCount) {
        return pair.getColumn() + moveCount;
    }

    public static int moveRightward(Pair pair, int moveCount) {
        return pair.getColumn() - moveCount;
    }

    public static int moveForward(Pair pair, int moveCount) {
        return pair.getRow() + moveCount;
    }

    public static int moveBackward(Pair pair, int moveCount) {
        return pair.getRow() - moveCount;
    }


}
