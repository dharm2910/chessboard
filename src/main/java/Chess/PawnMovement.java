package Chess;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thakurd on 13/02/21.
 */
public class PawnMovement implements PieceMovement {


    @Override
    public List<Pair> move(Pair currentPosition) {
        List<Pair> list = new ArrayList<>();
        if (MoveDeterminer.isForwardRowExistForMove(currentPosition, PAWN_MOVE)) {
            int forward = MoveDeterminer.moveForward(currentPosition, PAWN_MOVE);
            Pair updatedPosition = new Pair((char) forward, currentPosition.getColumn());
            list.add(updatedPosition);
        }
        return list;
    }
}