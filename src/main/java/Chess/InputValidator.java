package Chess;

/**
 * Created by thakurd on 16/02/21.
 */
public class InputValidator {

    public static String validate(PieceType pieceType, char row, int column) {

        StringBuilder stringBuilder = new StringBuilder();
        int count = 0;
        stringBuilder.append("--------------Invalid--Input---------------");
        stringBuilder.append("\n");
        if (pieceType == null) {
            count++;
            stringBuilder.append("Invalid pieceType it must be: { ");
            for (PieceType pieceType1 : PieceType.values()) {
                stringBuilder.append(pieceType1);
                stringBuilder.append(",");
            }
            stringBuilder.append("}");
            stringBuilder.append("\n");
        }
        if (row < Board.getStartRow() || row > Board.getMaxrow()) {
            count++;
            stringBuilder.append("Invalid second input Row it must be between " + Board.getStartRow() + " and " + Board.getMaxrow());
            stringBuilder.append("\n");
        }

        if (column < Board.getStartColumn() || column > Board.getMaxcolumn()) {
            count++;
            stringBuilder.append("Invalid third input Column it must be between " + Board.getStartColumn() + " and " + Board.getMaxcolumn());

        }
        stringBuilder.append("\n");
        stringBuilder.append("-------------------------------------------");
        return count < 1 ? "" : stringBuilder.toString();
    }
}
