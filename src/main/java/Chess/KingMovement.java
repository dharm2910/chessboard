package Chess;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by thakurd on 13/02/21.
 */
public class KingMovement implements PieceMovement {



    @Override
    public List<Pair> move(Pair currentPosition) {
        List<Pair> list = new ArrayList<>();
        if (MoveDeterminer.isForwardRowExistForMove(currentPosition, KING_MOVE)) {
            int forward = MoveDeterminer.moveForward(currentPosition, KING_MOVE);
            Pair pair = new Pair((char) forward, currentPosition.getColumn());
            if (!list.contains(pair)) {
                list.add(pair);
            }
            addMoveLeftFromHere(pair, list);
            addMoveRightFromHere(pair, list);
        }
        if (MoveDeterminer.isBackwardExistForMove(currentPosition, KING_MOVE)) {
            int backward = MoveDeterminer.moveBackward(currentPosition, KING_MOVE);
            Pair pair = new Pair((char) backward, currentPosition.getColumn());
            if (!list.contains(pair)) {
                list.add(pair);
            }
            addMoveLeftFromHere(pair, list);
            addMoveRightFromHere(pair, list);

        }
        addMoveLeftFromHere(currentPosition, list);
        addMoveRightFromHere(currentPosition, list);

        return list;

    }

    private void addMoveLeftFromHere(Pair thisPosition, List<Pair> list) {
        if (MoveDeterminer.isLeftColumnExistForMove(thisPosition, KING_MOVE)) {
            int leftward = MoveDeterminer.moveLeftward(thisPosition, KING_MOVE);
            Pair pair = new Pair(thisPosition.getRow(), leftward);
            if (!list.contains(pair)) {
                list.add(pair);
            }
        }
    }


    private void addMoveRightFromHere(Pair thisPosition, List<Pair> list) {
        if (MoveDeterminer.isRightColumnExistForMove(thisPosition, KING_MOVE)) {
            int righward = MoveDeterminer.moveRightward(thisPosition, KING_MOVE);
            Pair pair = new Pair(thisPosition.getRow(), righward);
            if (!list.contains(pair)) {
                list.add(pair);
            }
        }
    }
}