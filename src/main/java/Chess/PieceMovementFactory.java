package Chess;

/**
 * Created by thakurd on 15/02/21.
 */


import java.util.HashMap;
import java.util.Map;

import static Chess.PieceType.*;

public class PieceMovementFactory {

    private Map<PieceType, PieceMovement> map = new HashMap<>();

    private static PieceMovementFactory pieceMovementFactory;

    private PieceMovementFactory() {
        map.put(KING, new KingMovement());
        map.put(QUEEN, new QueenMovement());
        map.put(PAWN, new PawnMovement());
        map.put(HORSE, new HorseMovement());
        map.put(BISHOP, new BishopMovement());
        map.put(ROOK, new RookMovement());
    }

    public static PieceMovementFactory getInstance() {
        if (pieceMovementFactory == null) {
            synchronized (PieceMovementFactory.class) {
                if (pieceMovementFactory == null) {
                    pieceMovementFactory = new PieceMovementFactory();
                }
            }
        }
        return pieceMovementFactory;

    }

    public PieceMovement getMovement(PieceType pieceType) {
        return map.get(pieceType);
    }
}
