package Chess;

/**
 * Created by thakurd on 13/02/21.
 */
public class ChessMain {

    public static void main(String[] args) {

        if (!(args.length == 3)) {
            System.out.println("3Number of input required. <PieceType> <ROW> <Column>  eg. KING A 5");
            return;
        }
        PieceType pieceType = PieceType.parse(args[0]);
        char row = Character.valueOf(args[1].charAt(0));
        int column = Character.getNumericValue(args[2].charAt(0));
        String validateResult = InputValidator.validate(pieceType, row, column);
        if (!validateResult.isEmpty()) {
            System.out.println(validateResult);
            return;
        }
        Pair cuurentPosition = new Pair(row, column);
        System.out.println(PieceMovementFactory.getInstance().getMovement(pieceType).move(cuurentPosition));
    }


}
