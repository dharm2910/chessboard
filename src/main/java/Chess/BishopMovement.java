package Chess;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thakurd on 15/02/21.
 */
public class BishopMovement implements PieceMovement {

    @Override
    public   List<Pair> move(final Pair currentPosition) {
        List<Pair> list = new ArrayList<>();
        Pair thisPosition = currentPosition;//new Pair(currentPosition.getRow(),currentPosition.getColumn());
        while (MoveDeterminer.isForwardRowExistForMove(thisPosition, BISHOP_MOVE) && MoveDeterminer.isLeftColumnExistForMove(thisPosition, BISHOP_MOVE)) {
            int forward = MoveDeterminer.moveForward(thisPosition, BISHOP_MOVE);
            Pair intermidiatePosition = new Pair((char) forward, thisPosition.getColumn());
            int leftward = MoveDeterminer.moveLeftward(intermidiatePosition, BISHOP_MOVE);
            thisPosition = new Pair(intermidiatePosition.getRow(), leftward);
            list.add(thisPosition);
        }
        thisPosition = currentPosition;
        while (MoveDeterminer.isForwardRowExistForMove(thisPosition, BISHOP_MOVE) && MoveDeterminer.isRightColumnExistForMove(thisPosition, BISHOP_MOVE)) {
            int forward = MoveDeterminer.moveForward(thisPosition, BISHOP_MOVE);
            Pair intermidiatePosition = new Pair((char) forward, thisPosition.getColumn());
            int rightward = MoveDeterminer.moveRightward(intermidiatePosition, BISHOP_MOVE);
            thisPosition = new Pair(intermidiatePosition.getRow(), rightward);
            list.add(thisPosition);
        }

        thisPosition = currentPosition;
        while (MoveDeterminer.isBackwardExistForMove(thisPosition, BISHOP_MOVE) && MoveDeterminer.isLeftColumnExistForMove(thisPosition, BISHOP_MOVE)) {
            int backward = MoveDeterminer.moveBackward(thisPosition, BISHOP_MOVE);
            Pair intermidiatePosition = new Pair((char) backward, thisPosition.getColumn());
            int leftward = MoveDeterminer.moveLeftward(intermidiatePosition, BISHOP_MOVE);
            thisPosition = new Pair(intermidiatePosition.getRow(), leftward);
            list.add(thisPosition);
        }
        thisPosition = currentPosition;
        while (MoveDeterminer.isBackwardExistForMove(thisPosition, BISHOP_MOVE) && MoveDeterminer.isRightColumnExistForMove(thisPosition, BISHOP_MOVE)) {
            int backward = MoveDeterminer.moveBackward(thisPosition, BISHOP_MOVE);
            Pair intermidiatePosition = new Pair((char) backward, thisPosition.getColumn());
            int rightward = MoveDeterminer.moveRightward(intermidiatePosition, BISHOP_MOVE);
            thisPosition = new Pair(intermidiatePosition.getRow(), rightward);
            list.add(thisPosition);
        }
        return list;
    }
}
