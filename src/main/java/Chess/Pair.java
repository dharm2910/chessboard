package Chess;

/**
 * Created by thakurd on 13/02/21.
 */
public class Pair {
    char row;

    int column;

    Pair(){

    }
    @Override
    public String toString() {
        return row + "" + column + "";

    }

    public char getRow() {
        return row;
    }

    @Override
    public boolean equals(Object obj) {
        Pair pair = (Pair) obj;
        return this.toString().equals(pair.toString());
    }

    public void setRow(char row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public Pair(char row, int column) {
        this.row = row;
        this.column = column;
    }

    public void setColumn(int column) {
        this.column = column;
    }
}
