package Chess;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thakurd on 13/02/21.
 */
public class RookMovement implements PieceMovement {

    @Override
    public List<Pair> move(Pair currentPosition) {
        List<Pair> list = new ArrayList<>();

        for (char i = Board.getStartRow(); i <= Board.getMaxrow(); i++) {
            Pair pair = new Pair();
            pair.setColumn(currentPosition.getColumn());
            if (i == currentPosition.getRow()) {
                continue;
            }
            pair.setRow(i);
            list.add(pair);

        }
        for (int i = Board.getStartColumn(); i <= Board.getMaxcolumn(); i++) {
            Pair pair = new Pair();
            pair.setRow(currentPosition.getRow());
            if (i == currentPosition.getColumn()) {
                continue;
            }
            pair.setColumn(i);
            list.add(pair);

        }
        return list;

    }
}
