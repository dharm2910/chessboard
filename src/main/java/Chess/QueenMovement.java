package Chess;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thakurd on 15/02/21.
 */
public class QueenMovement implements PieceMovement {


    @Override
    public List<Pair> move(Pair currentPosition) {
        List<Pair> list = new ArrayList<>();

        BishopMovement bishopMovement = new BishopMovement();
        List<Pair> listOfBishopMoves = bishopMovement.move(currentPosition);

        RookMovement rookMovement = new RookMovement();
        List<Pair> listOfrookMoves = rookMovement.move(currentPosition);

        listOfBishopMoves.addAll(listOfrookMoves);
        list.addAll(listOfBishopMoves);
        return list;


    }

}
