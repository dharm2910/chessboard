package Chess;

/**
 * Created by thakurd on 13/02/21.
 */
public enum PieceType {


    KING("King"),
    QUEEN("Queen"),
    BISHOP("Bishop"),
    ROOK("Rook"),
    HORSE("Horse"),
    PAWN("Pawn");

    private String value;

    private PieceType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static PieceType parse(String type) {
        for (PieceType pieceType : PieceType.values()) {
            if (pieceType.getValue().equalsIgnoreCase(type)) {
                return pieceType;
            }
        }
        return null;
    }
}
