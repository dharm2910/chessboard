package Chess;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thakurd on 14/02/21.
 */
public class HorseMovement implements PieceMovement {

    @Override
    public List<Pair> move(Pair currentPosition) {
        List<Pair> list = new ArrayList<>();
        list.addAll(moveForward(currentPosition));
        list.addAll(moveBackward(currentPosition));
        list.addAll(moveLeftward(currentPosition));
        list.addAll(moveRightward(currentPosition));
        return list;
    }

    private List<Pair> moveForward(Pair currentPosition) {
        List<Pair> list = new ArrayList<>();
        if (MoveDeterminer.isForwardRowExistForMove(currentPosition, HORSE_FISRT_MOVE)) {
            int forward = MoveDeterminer.moveForward(currentPosition, HORSE_FISRT_MOVE);
            if (MoveDeterminer.isLeftColumnExistForMove(currentPosition, HORSE_SECOND_MOVE)) {
                int leftward = MoveDeterminer.moveLeftward(currentPosition, HORSE_SECOND_MOVE);
                Pair pair = new Pair((char) forward, leftward);
                list.add(pair);
            }
            if (MoveDeterminer.isRightColumnExistForMove(currentPosition, HORSE_SECOND_MOVE)) {
                int rightward = MoveDeterminer.moveRightward(currentPosition, HORSE_SECOND_MOVE);
                Pair pair = new Pair((char) forward, rightward);
                list.add(pair);
            }

        }
        return list;
    }

    private List<Pair> moveBackward(Pair currentPosition) {
        List<Pair> list = new ArrayList<>();
        if (MoveDeterminer.isBackwardExistForMove(currentPosition, HORSE_FISRT_MOVE)) {
            int backward = MoveDeterminer.moveBackward(currentPosition, HORSE_FISRT_MOVE);
            if (MoveDeterminer.isLeftColumnExistForMove(currentPosition, HORSE_SECOND_MOVE)) {
                int leftward = MoveDeterminer.moveLeftward(currentPosition, HORSE_SECOND_MOVE);
                Pair pair = new Pair((char) backward, leftward);
                list.add(pair);
            }
            if (MoveDeterminer.isRightColumnExistForMove(currentPosition, HORSE_SECOND_MOVE)) {
                int rightward = MoveDeterminer.moveRightward(currentPosition, HORSE_SECOND_MOVE);
                Pair pair = new Pair((char) backward, rightward);
                list.add(pair);
            }

        }
        return list;
    }

    private List<Pair> moveLeftward(Pair currentPosition) {
        List<Pair> list = new ArrayList<>();
        if (MoveDeterminer.isLeftColumnExistForMove(currentPosition, HORSE_FISRT_MOVE)) {
            int leftward = MoveDeterminer.moveLeftward(currentPosition, HORSE_FISRT_MOVE);
            if (MoveDeterminer.isForwardRowExistForMove(currentPosition, HORSE_SECOND_MOVE)) {
                int forward = MoveDeterminer.moveForward(currentPosition, HORSE_SECOND_MOVE);
                Pair pair = new Pair((char) forward, leftward);
                list.add(pair);
            }
            if (MoveDeterminer.isBackwardExistForMove(currentPosition, HORSE_SECOND_MOVE)) {
                int forward = MoveDeterminer.moveBackward(currentPosition, HORSE_SECOND_MOVE);
                Pair pair = new Pair((char) forward, leftward);
                list.add(pair);
            }
        }
        return list;
    }

    private List<Pair> moveRightward(Pair currentPosition) {
        List<Pair> list = new ArrayList<>();
        if (MoveDeterminer.isRightColumnExistForMove(currentPosition, HORSE_FISRT_MOVE)) {
            int rightward = MoveDeterminer.moveRightward(currentPosition, HORSE_FISRT_MOVE);
            if (MoveDeterminer.isForwardRowExistForMove(currentPosition, HORSE_SECOND_MOVE)) {
                int forward = MoveDeterminer.moveForward(currentPosition, HORSE_SECOND_MOVE);
                Pair pair = new Pair((char) forward, rightward);
                list.add(pair);
            }
            if (MoveDeterminer.isBackwardExistForMove(currentPosition, HORSE_SECOND_MOVE)) {
                int forward = MoveDeterminer.moveBackward(currentPosition, HORSE_SECOND_MOVE);
                Pair pair = new Pair((char) forward, rightward);
                list.add(pair);
            }
        }
        return list;
    }

}




