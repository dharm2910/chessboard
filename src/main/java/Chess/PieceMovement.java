package Chess;

import java.util.List;

/**
 * Created by thakurd on 15/02/21.
 */
public interface PieceMovement {
    int PAWN_MOVE = 1;
    int KING_MOVE = 1;
    int HORSE_FISRT_MOVE = 2;
    int HORSE_SECOND_MOVE = 1;
    int BISHOP_MOVE = 1;

    List<Pair> move(Pair currentPosition);


}
