package Chess;

/**
 * Created by thakurd on 13/02/21.
 */
public class Board {

    static char startRow = 'A';
    static int startColumn = 1;
    static char row = 'H';

    static int column = 8;

    public static int getMaxcolumn() {
        return column;
    }


    public static char getMaxrow() {
        return row;
    }


    public static int getStartColumn() {
        return startColumn;
    }

    public static void setStartColumn(int startColumn) {
        Board.startColumn = startColumn;
    }

    public static char getStartRow() {
        return startRow;
    }

    public static void setStartRow(char startRow) {
        Board.startRow = startRow;
    }


    Board(char row, int column) {
        this.row = row;
        this.column = column;
    }

}
