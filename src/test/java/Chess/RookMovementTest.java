package Chess;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by thakurd on 17/02/21.
 */
public class RookMovementTest {

    private RookMovement movement = new RookMovement();

    @Test
    public void testWhenNoRightMoveExistInFirstRow() {
        Pair pair = new Pair('A', 1);
        List<Pair> list = movement.move(pair);
        Assert.assertEquals("[B1, C1, D1, E1, F1, G1, H1, A2, A3, A4, A5, A6, A7, A8]", list.toString());
    }

    @Test
    public void testWhenNoLeftMoveExistInFirstRow() {
        Pair pair = new Pair('A', 8);
        List<Pair> list = movement.move(pair);
        Assert.assertEquals("[B8, C8, D8, E8, F8, G8, H8, A1, A2, A3, A4, A5, A6, A7]", list.toString());

    }


    @Test
    public void testWhenNoForwardAndNoLeftdExist() {
        Pair pair = new Pair('H', 8);
        List<Pair> list = movement.move(pair);
        Assert.assertEquals("[A8, B8, C8, D8, E8, F8, G8, H1, H2, H3, H4, H5, H6, H7]",list.toString());

    }



    @Test
    public void testWhenNoForwardAndNoRightExist() {
        Pair pair = new Pair('H', 1);
        List<Pair> list = movement.move(pair);
        Assert.assertEquals("[A1, B1, C1, D1, E1, F1, G1, H2, H3, H4, H5, H6, H7, H8]",list.toString());

    }

    @Test
    public void testWhenAllDirectionOpenToMove() {
        Pair pair = new Pair('D', 5);
        List<Pair> list = movement.move(pair);
        Assert.assertEquals("[A5, B5, C5, E5, F5, G5, H5, D1, D2, D3, D4, D6, D7, D8]", list.toString());
    }
}