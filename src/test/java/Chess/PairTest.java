package Chess;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by thakurd on 17/02/21.
 */
public class PairTest {


    @Test
    public void testToString() throws Exception {
        Pair pair = new Pair('A', 6);
        Assert.assertEquals("A6", pair.toString());
    }

    @Test
    public void testUnEquals() throws Exception {
        Pair pair = new Pair('A', 6);
        Pair pair2 = new Pair('B', 6);
        Assert.assertFalse(pair.equals(pair2));

    }
    @Test
    public void testEqualsTrue() throws Exception {
        Pair pair = new Pair('A', 6);
        Pair pair2 = new Pair('A', 6);
        Assert.assertTrue(pair.equals(pair2));

    }
}