package Chess;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by thakurd on 17/02/21.
 */
public class PawnMovementTest {

    private PawnMovement movement = new PawnMovement();


    @Test
    public void testWhenNoRightMoveExistInFirstRow() {
        Pair pair = new Pair('A', 1);
        List<Pair> list = movement.move(pair);
        Assert.assertEquals("[B1]", list.toString());
    }

    @Test
    public void testWhenNoLeftMoveExistInFirstRow() {
        Pair pair = new Pair('A', 8);
        List<Pair> list = movement.move(pair);
        Assert.assertEquals("[B8]", list.toString());

    }


    @Test
    public void testWhenNoForwardAndNoLeftdExist() {
        Pair pair = new Pair('H', 8);
        List<Pair> list = movement.move(pair);
        Assert.assertEquals(0,list.size());

    }



    @Test
    public void testWhenNoForwardAndNoRightExist() {
        Pair pair = new Pair('H', 1);
        List<Pair> list = movement.move(pair);
        Assert.assertEquals(0,list.size());
    }

    @Test
    public void testWhenAllDirectionOpenToMove() {
        Pair pair = new Pair('D', 5);
        List<Pair> list = movement.move(pair);
        Assert.assertEquals("[E5]", list.toString());
    }
}