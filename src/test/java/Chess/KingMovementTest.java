package Chess;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by thakurd on 17/02/21.
 */
public class KingMovementTest {
    KingMovement movement = new KingMovement();

    @Test
    public void testWhenNoRightMoveExistInFirstRow() {
        Pair pair = new Pair('A', 1);
        List<Pair> list = movement.move(pair);
        Assert.assertEquals("[B1, B2, A2]", list.toString());
    }

    @Test
    public void testWhenNoLeftMoveExistInFirstRow() {
        Pair pair = new Pair('A', 8);
        List<Pair> list = movement.move(pair);
        Assert.assertEquals("[B8, B7, A7]", list.toString());

    }


    @Test
    public void testWhenNoForwardAndNoLeftdExist() {
        Pair pair = new Pair('H', 8);
        List<Pair> list = movement.move(pair);
        Assert.assertEquals("[G8, G7, H7]", list.toString());

    }


    @Test
    public void testWhenNoForwardAndNoRightExist() {
        Pair pair = new Pair('H', 1);
        List<Pair> list = movement.move(pair);
        Assert.assertEquals("[G1, G2, H2]", list.toString());
    }

    @Test
    public void testWhenAllDirectionOpenToMove() {
        Pair pair = new Pair('D', 5);
        List<Pair> list = movement.move(pair);
        Assert.assertEquals("[E5, E6, E4, C5, C6, C4, D6, D4]", list.toString());
    }
}