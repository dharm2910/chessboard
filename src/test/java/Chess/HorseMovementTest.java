package Chess;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by thakurd on 17/02/21.
 */
public class HorseMovementTest {

    private HorseMovement movement = new HorseMovement();


    @Test
    public void testWhenNoRightMoveExistInFirstRow() {
        Pair pair = new Pair('A', 1);
        List<Pair> list= movement.move(pair);
        System.out.println(list.toString());
        Assert.assertEquals("[C2, B3]", list.toString());
    }
    @Test
    public void testWhenNoLeftMoveExistInFirstRow() {
        Pair pair = new Pair('A', 8);
        List<Pair> list= movement.move(pair);
        Assert.assertEquals("[C7, B6]", list.toString());
    }



    @Test
    public void testWhenNoForwardAndNoLeftdExist() {
        Pair pair = new Pair('H', 8);
        List<Pair> list= movement.move(pair);
        Assert.assertEquals("[F7, G6]", list.toString());
    }

    @Test
    public void testWhenNoForwardAndNoRightExist() {
        Pair pair = new Pair('H', 1);
        List<Pair> list= movement.move(pair);
        Assert.assertEquals("[F2, G3]", list.toString());
    }

    @Test
    public void testWhenAllDirectionOpenToMove() {
        Pair pair = new Pair('D', 5);
        List<Pair> list= movement.move(pair);
        Assert.assertEquals("[F6, F4, B6, B4, E7, C7, E3, C3]", list.toString());
    }

    @Test
    public void test() {
        Pair pair = new Pair('D', 8);
        List<Pair> list= movement.move(pair);
        Assert.assertEquals("[F7, B7, E6, C6]", list.toString());
    }


}