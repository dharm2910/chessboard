package Chess;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by thakurd on 16/02/21.
 */
public class BishopMovementTest {

    private BishopMovement bishopMovement = new BishopMovement();

    @Test
    public void testWhenNoRightMoveExistInFirstRow() {
        Pair pair = new Pair('A', 1);
        List<Pair> list= bishopMovement.move(pair);
        Assert.assertEquals(list.size(),7);
        Assert.assertEquals("[B2, C3, D4, E5, F6, G7, H8]", list.toString());
    }
    @Test
    public void testWhenNoLeftMoveExistInFirstRow() {
        Pair pair = new Pair('A', 8);
        List<Pair> list= bishopMovement.move(pair);
        Assert.assertEquals(list.size(),7);
        Assert.assertEquals("[B7, C6, D5, E4, F3, G2, H1]", list.toString());
    }



       @Test
    public void testWhenNoForwardAndNoLeftdExist() {
        Pair pair = new Pair('H', 8);
        List<Pair> list= bishopMovement.move(pair);
        Assert.assertEquals("[G7, F6, E5, D4, C3, B2, A1]", list.toString());
    }

    @Test
    public void testWhenNoForwardAndNoRightExist() {
        Pair pair = new Pair('H', 1);
        List<Pair> list= bishopMovement.move(pair);
        Assert.assertEquals("[G2, F3, E4, D5, C6, B7, A8]", list.toString());
    }

    @Test
    public void testWhenAllDirectionOpenToMove() {
        Pair pair = new Pair('D', 5);
        List<Pair> list= bishopMovement.move(pair);
        Assert.assertEquals("[E6, F7, G8, E4, F3, G2, H1, C6, B7, A8, C4, B3, A2]", list.toString());
    }


}