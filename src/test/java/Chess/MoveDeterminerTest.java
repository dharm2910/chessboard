package Chess;


import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by thakurd on 17/02/21.
 */
public class MoveDeterminerTest {


    @Test
    public void testIsLeftColumnExistForMove() {
        Pair pair = new Pair(Board.getStartRow(), Board.getMaxcolumn());
        assertFalse(MoveDeterminer.isLeftColumnExistForMove(pair, 1));

    }

    @Test
    public void testIsRightColumnExistForMove() {
        Pair pair = new Pair(Board.getStartRow(), Board.getStartColumn());
        assertFalse(MoveDeterminer.isRightColumnExistForMove(pair, 1));


    }

    @Test
    public void testIsForwardRowExistForMove() {
        Pair pair = new Pair(Board.getMaxrow(), Board.getStartColumn());
        assertFalse(MoveDeterminer.isRightColumnExistForMove(pair, 1));


    }

    @Test
    public void testIsBackwardExistForMove() {
        Pair pair = new Pair(Board.getStartRow(), Board.getStartColumn());
        assertFalse(MoveDeterminer.isRightColumnExistForMove(pair, 1));

    }


    @Test
    public void testMoveRightward() {
        Pair pair = new Pair('D', 5);
        Assert.assertEquals(4, MoveDeterminer.moveRightward(pair, 1));
    }

    @Test
    public void testMoveForward() {
        Pair pair = new Pair('D', 5);
        Assert.assertEquals('E', MoveDeterminer.moveForward(pair, 1));
    }

    @Test
    public void testMoveBackward() {
        Pair pair = new Pair('D', 5);
        Assert.assertEquals('C', MoveDeterminer.moveBackward(pair, 1));
    }

    @Test
    public void moveLeftward() {
        Pair pair = new Pair('D', 5);
        Assert.assertEquals(6, MoveDeterminer.moveLeftward(pair, 1));

    }

}