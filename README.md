**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---
## ChessBorad
The chessboard is an 8 x 8 grid with 64 cells in it.
With 8 rows (A, B, C…. H) and 8 columns (1, 2, 3…. 8), each cell can be uniquely
identified with its cell number. 

Chess pieces and their movements:
The game of chess has 6 unique types of pieces, with their own unique types
of movements. These are:
1.) King – Can move only 1 step at a time in all 8 directions (horizontal, vertical
and diagonal)
2.) Queen – Can move across the board in all 8 directions
3.) Bishop – Can move across the board only diagonally
4.) Horse – Can move across the board only in 2.5 steps (2 vertical steps and 1
horizontal step)
5.) Rook – Can move across the board only vertically and horizontally
6.) Pawn – Can move only 1 step at a time, in the forward direction, vertically.
Can also move 1 step forward diagonally, in order to eliminate an opposing
piece.

Objective of your program:
Your program should simulate the movement of each unique chess piece on an
empty chessboard.

• Input – The input string to your program will be the Type of chess piece and
its Position (cell number) on the chessboard. E.g. “King D5”

• Output – Once you execute the program, the output will be a string of all
possible cells in which the chess piece can move.


Sample inputs and outputs:
Input – “King D5”
Output – “D6, E6, E5, E4, D4, C4, C5, C6”

Assumption:
Assume that the board is empty. This means that the pawn cannot move
diagonally.

---
## How to use:

Its a maven based project. to build and create jar mvn commands can be used.

eg. **mvn clean install**
it would place the file in target folder of this project.


**java -jar chessBoad-1.0-SNAPSHOT.jar KING B 3**

Input are separated by space after .jar file in the above command.

**Input one** is one of the piece type {KING,QUEEN,BISHOP,ROOK,HORSE,PAWN} 

**Input two** is rows (A, B, C…. H) 

**Input three** is columns (1, 2, 3…. 8)

ChessMain.java is starter Main class. 
The executable jar file is added into the source code.

https://bitbucket.org/dharm2910/chessboard/src/master/chessBoad-1.0-SNAPSHOT.jar

--- 
**maven depedency and plugin used:**

junit depedency
maven-jar-plugin  





---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).